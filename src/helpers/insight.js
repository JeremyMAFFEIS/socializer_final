var insight = new Vue({
    el: '#insight',
    data: {
      h3: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
      h1: 'Socializer Insight',
      p: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil perspiciatis vero perferendis enim rerum, harum neque mollitia consequuntur dolor aspernatur accusamus modi. Quas eveniet tempora omnis soluta dolorum voluptatibus praesentium!.',
      button: 'Schedule a demo',
      image: 'ressources/img/illustration.png'
    }
})