Vue.component('signin-component', {
    props: ['signin'],
    template: ''
})

var signin = new Vue({
    el: '#v',
    data: {
      signins: [
        { text: 'Utilisateur / Mail',
        placeholder: 'Utilisateur ou adresse mail'},
        { text: 'Mot de passe',
        placeholder: 'Mot de passe'}
      ]
    }.$mount('#app')
})