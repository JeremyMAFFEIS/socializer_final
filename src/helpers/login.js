Vue.component('login-component', {
    props: ['login'],
    template: '<div class="row"><div class="col-md-6 col-xs-12"><div class="form-group"><label for="">{{ login.text }}</label><input type="text" class="form-control" required="required" :placeholder=" login.placeholder "></div></div></div>'
})

var login = new Vue({
    el: '#login',
    data: {
      logins: [
        { text: 'Utilisateur / Mail',
        placeholder: 'Utilisateur ou adresse mail'},
        { text: 'Mot de passe',
        placeholder: 'Mot de passe'}
      ]
    }
})