Vue.component('mon-menu', {
    props: ['menu'],
    template: '<li class="nav-item"><a :href=" menu.lien " class="nav-link">{{ menu.text }}</a></li>'
})

var menu = new Vue({
    el: '#menu',
    data: {
      menus: [
        { id: 0, text: 'Why Socialize', lien:"#" },
        { id: 1, text: 'Features', lien:"#" },
        { id: 2, text: 'Pricing', lien:"#"},
        { id: 3, text: 'Register', lien:"src/pages/sign-in.html" }
      ]
    }
})