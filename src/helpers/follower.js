Vue.component('followers-component', {
    template: '<div class="interface-item"><div class="interface-thumbnail"><img :src="image" alt=""></div><div class="interface-item--content"><h3>{{h3}}</h3><p>{{p}}</p></div></div>'
})

var followers = new Vue({
    el: '#followers',
    data: {
      content: [
        { h3: 'Forecast',
        p: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae laborum soluta porro repellat, neque quos! Nisi atque odio dolorum voluptatum inventore',
        image: 'ressources/img/screen1.png'},
        { h3: 'Visualization',
        p: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae laborum soluta porro repellat, neque quos! Nisi atque odio dolorum voluptatum inventore',
        image: 'ressources/img/screen2.png'},
        { h3: 'Improvment',
        p: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae laborum soluta porro repellat, neque quos! Nisi atque odio dolorum voluptatum inventore',
        image: 'ressources/img/screen3.png'},
      ]
    }
})