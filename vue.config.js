module.exports = {
    pages: {
      'index': {
        // entry for the page
        entry: 'src/pages/index/main.js',
        // the source template
        template: 'public/index.html',
        // output as dist/index.html
        filename: 'index.html',
        // when using title option,
        // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
        title: 'Index Page',
        // chunks to include on this page, by default includes
        // extracted common chunks and vendor chunks.
        chunks: ['chunk-vendors', 'chunk-common', 'index']
      },
      'dashboard': {
        entry: 'src/pages/dashboard/main.js',
        template: 'public/dashboard.html',
        title: 'Dashboard',
        chunks: ['chunk-vendors', 'chunk-common', 'dashboard']
      },
      'login': {
        entry: 'src/pages/login/main.js',
        template: 'public/login.html',
        title: 'Login',
        chunks: ['chunk-vendors', 'chunk-common', 'login']
      },
      'sign-in': {
        entry: 'src/pages/sign-in/main.js',
        template: 'public/sign-in.html',
        title: 'Sign in',
        chunks: ['chunk-vendors', 'chunk-common', 'sign-in']
      }
    }
  }