// main.js

import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import AddUser from '../../components/AddUser.vue';
Vue.use(VueRouter);

import axios from 'axios';
import VueAxios from 'vue-axios';
import UserView from '../../components/UserView.vue';

Vue.use(VueAxios, axios);

import Login from '../../components/login.vue'

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    name: 'add',
    path: '/add',
    component: AddUser
  },
  {
    name: 'index',
    path: '/index',
    component: UserView
  }
];
const router = new VueRouter({ mode: 'history', routes: routes });

Vue.config.productionTip = false;

var login = new Vue({
  render: h => h(Login),
  router
}).$mount('#login')